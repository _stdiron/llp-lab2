%{
    #include <stdio.h>
    #include "parser.tab.h"
    #include "ast.h"

//     #define YYDEBUG 1

    int yylex();
    void yyerror(struct Ast** tree, const char *s);

%}

%code requires {
    struct Ast* parse_ast(char* input);
}

%union {
    struct Ast* a;
    float f;
    int i;
    char* s;
}

%token <s> VARNAME STRING
%token <i> INT BOOL CMP VARTYPE
%token <f> FLOAT
%token INSERT IN RETURN UPDATE WITH REMOVE FILTER MERGE FOR CREATE
%token EOL INDENT UNINDENT YYEOF WRONG_INDENT

%left ','
%left '|'
%left '&'
%left IN CMP
%left '.'

%type <a> value condition kv_pair definition
%type <a> document return update filter kv_pairs
%type <a> args merge insert remove for
%type <a> expr expr_list scope create
%type <a> variable definitions code

%locations
%start code
%parse-param { struct Ast** tree }
%define parse.error verbose

%%

code: expr_list
    | EOL expr_list { $$ = $2; }
    ;

scope: EOL INDENT expr_list UNINDENT    { $$ = $3; }
        | EOL INDENT expr_list YYEOF    { $$ = $3; }
        ;

expr_list: expr
        | expr_list expr { add_at_end($$, $2); }
        | expr_list EOL
        ;

expr: filter eol
    | update eol
    | merge eol
    | return eol
    | remove eol
    | insert eol
    | create eol
    | for
    ;

eol: EOL
    | YYEOF
    ;

for: FOR VARNAME IN VARNAME scope       { $$ = create_dstring_child_node(NT_FOR, $2, $4, $5); *tree = $$; }
     ;

remove: REMOVE VARNAME IN VARNAME       { $$ = create_dstring_node(NT_REMOVE, $2, $4); *tree = $$; }
        ;

insert: INSERT document IN VARNAME      { $$ = create_string_child_node(NT_INSERT, $4, $2); *tree = $$; }
        ;

merge:  MERGE args ')'                  { $$ = create_node(NT_MERGE, $2, 0); *tree = $$;}

args: variable
        | args ',' variable             { add_at_end($$, $3); }
        ;

filter: FILTER condition                { $$ = create_node(NT_FILTER, $2, 0); *tree = $$; }
        ;

condition: '(' condition ')'            { $$ = $2; *tree = $$; }
        | condition '&' condition       { $$ = create_node(NT_COND_AND, $1, $3); *tree = $$; }
        | condition '|' condition       { $$ = create_node(NT_COND_OR, $1, $3); *tree = $$; }
        | value CMP value               { $$ = create_node($2, $1, $3); *tree = $$; }
        | value IN value                { $$ = create_node(NT_COND_SUBSTR, $1, $3); *tree = $$; }
        ;

update: UPDATE VARNAME WITH document IN VARNAME { $$ = create_dstring_child_node(NT_UPDATE, $2, $6, $4); *tree = $$; }
        ;

return: RETURN value       { $$ = create_node(NT_RET, $2, 0); *tree = $$; }
        | RETURN merge     { $$ = create_node(NT_RET, $2, 0); *tree = $$; }
        ;

document: '{' kv_pairs '}'  { $$ = $2; *tree = $$; }
        ;

create: CREATE VARNAME WITH '{' definitions '}' { $$ = create_string_child_node(NT_CREATE, $2, $5); *tree = $$; }
        ;

definitions: definition
            | definitions ',' definition        { add_at_end($$, $3); }

definition:  VARNAME ':' VARTYPE                { $$ = create_string_node($1, $3); *tree = $$; }
        ;

kv_pairs: kv_pair
    | kv_pairs ',' kv_pair                      { add_at_end($$, $3); }
    ;

kv_pair:  VARNAME ':' value { $$ = create_string_child_node(NT_PAIR, $1, $3); *tree = $$; }
        ;

value: STRING   { $$ = create_string_node($1, NT_STR); *tree = $$; }
    | INT       { $$ = create_int_node($1); *tree = $$; }
    | FLOAT     { $$ = create_float_node($1); *tree = $$; }
    | BOOL      { $$ = create_bool_node($1); *tree = $$; }
    | variable
    ;

variable: VARNAME               { $$ = create_string_node($1, NT_VAR); *tree = $$; }
        | VARNAME '.' VARNAME   { $$ = create_dstring_node(NT_POINT_OP, $1, $3); *tree = $$; }
        ;


%%

typedef struct yy_buffer_state * YY_BUFFER_STATE;
extern YY_BUFFER_STATE yy_scan_string(char * str);
extern void yy_delete_buffer(YY_BUFFER_STATE buffer);

struct Ast* parse_ast(char* input) {
    struct Ast* tree = 0;
    YY_BUFFER_STATE buffer = yy_scan_string(input);
    yyparse(&tree);
    yy_delete_buffer(buffer);
    return tree;
}