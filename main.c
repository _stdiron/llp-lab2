#include <stdio.h>
#include "parser.tab.h"
#include "ast.h"

extern int yydebug;


int main(int argc, char **argv)
{
//     yydebug = 1;
    char* str = malloc(512);
    while (1) {
        memset(str, 0, 512);
        if (scanf("%512[^;]c", str) <= 0)
            break;
        while (getchar() != ';') ;
        if (str[0] == '\0' || str[1] == '\0')
            break;
        struct Ast* tree = parse_ast(str);
        if (tree) {
            print_ast(stdout, tree, 0);
            free_ast(tree);
        }
    }
    free(str);
    return 0;
}

