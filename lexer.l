%{
    #include "ast.h"
    #include "parser.tab.h"
    #include <stdio.h>

    #define YY_USER_ACTION \
        yylloc.first_line = yylloc.last_line; \
        yylloc.first_column = yylloc.last_column; \
        for(int i = 0; yytext[i] != '\0'; i++) { \
            if(yytext[i] == '\n') { \
                yylloc.last_line++; \
                yylloc.last_column = 0; \
            } \
            else { \
                yylloc.last_column++; \
            } \
        }

    extern YYSTYPE yylval;

    int current_line_indent = 0;   /* indentation of the current line */
    int indent_level = 0;          /* indentation level passed to the parser */

    void yyerror(struct Ast** tree, const char *s) {
        // char* error_hint = malloc(yylloc.last_column+1);
        // error_hint[yylloc.last_column] = '\0';
        // memset(error_hint, ' ', yylloc.first_column);
        // memset(error_hint+yylloc.first_column-1, '^', yylloc.last_column-yylloc.first_column);
        fprintf(stderr, "Error at line %d columns from %d to %d:\n%s.",
          yylloc.first_line, yylloc.first_column, yylloc.last_column, s);
        // free(error_hint);
        fprintf(stderr, "\n");
        free_ast(*tree);
        current_line_indent = 0;
        indent_level = 0;
        memset(&yylloc, 0, sizeof(yylloc));
        *tree = 0;
    }

    int yywrap(void) {
        current_line_indent = 0;
        indent_level = 0;
        memset(&yylloc, 0, sizeof(yylloc));
        return 1;
    }

%}

%option caseless

%x indent 

%%
<indent>" "     { current_line_indent++; }
<indent>\t      { current_line_indent = (current_line_indent + 4) & ~3; }
<indent>\n      { current_line_indent = 0; }
<indent>.       {
                  unput(*yytext);
                  if (current_line_indent % 4 != 0) {
                    return WRONG_INDENT;
                  }
                  if ((current_line_indent / 4) > indent_level) {
                    indent_level++;
                    return INDENT;
                  } else if ((current_line_indent / 4) < indent_level) {
                    indent_level--;
                    return UNINDENT;
                  } else {
                    BEGIN INITIAL;
                  }
                }

<INITIAL>\n     { current_line_indent = 0; BEGIN indent; return EOL; }

"insert"  { return INSERT; }
"in"      { return IN; }
"return"  { return RETURN; }
"update"  { return UPDATE; }
"with"    { return WITH; }
"remove"  { return REMOVE; }
"filter"  { return FILTER; }
"merge("  { return MERGE; }
"for"     { return FOR; }
"create"  { return CREATE; }

"string"  { yylval.i = NT_DEF_STR; return VARTYPE; }
"int" { yylval.i = NT_DEF_INT; return VARTYPE; }
"bool"    { yylval.i = NT_DEF_BOOL; return VARTYPE; }
"float"   { yylval.i = NT_DEF_FLOAT; return VARTYPE; }

">"   { yylval.i = NT_COND_GR; return CMP; }
"<"   { yylval.i = NT_COND_LE; return CMP; }
"!="  { yylval.i = NT_COND_NEQ; return CMP; }
"=="  { yylval.i = NT_COND_EQ; return CMP; }
">="  { yylval.i = NT_COND_EGR; return CMP; }
"<="  { yylval.i = NT_COND_ELE; return CMP; }

"(" |
")" |
"&" |
"|" |
"{" |
"}" |
":" |
","   { return yytext[0]; }

"true"  { yylval.i = 1; return BOOL; }
"false" { yylval.i = 0; return BOOL; }

-?[0-9]+"."[0-9]*  {
  yylval.f = atof(yytext);
  return FLOAT;
}

"." { return yytext[0]; }

-?[0-9]+ {
  yylval.i = atoi(yytext); 
  return INT;
}

\"[^\""]*\"                       { yylval.s = strdup(yytext); return STRING; }
[a-zA-Z0-9_]+                   { yylval.s = strdup(yytext); return VARNAME; }


[ \t]+  {}
.       {  }
%%
