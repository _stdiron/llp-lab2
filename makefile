FNAME=aql_parser

.PHONY: all, clean
default: all

parser.tab.c: parser.y 
	bison -d parser.y

lex.yy.c: lexer.l parser.tab.c
	flex lexer.l

all: parser.tab.c lex.yy.c ast.h
	gcc -o $(FNAME) parser.tab.c lex.yy.c ast.c main.c -lfl -ly

debug: parser.tab.c lex.yy.c ast.h
	gcc -o $(FNAME) -g parser.tab.c lex.yy.c ast.c main.c -lfl -ly

valgrind: debug
	valgrind --leak-check=yes --leak-resolution=med --track-origins=yes ./$(FNAME)

clean: 
	rm parser.tab.* lex.yy.c $(FNAME) -f

